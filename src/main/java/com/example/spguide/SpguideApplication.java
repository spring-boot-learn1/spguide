package com.example.spguide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpguideApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpguideApplication.class, args);
	}

}
